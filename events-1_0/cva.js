// noinspection SpellCheckingInspection,JSUnresolvedVariable,JSUnresolvedFunction

const MELEE = 'melee';
const MAGIC = 'magic';
const RANGE = 'range';

const DPS = 'dps';
const SUPPORT = 'support';
const TANK = 'tank';
const HEALING = 'healing';
const GOLD = 'gold';
const SPEED = 'speed';
const DEBUFF = 'debuff';

function melee() {
    if (this.toString() === MELEE) {
        return champions.attackTypes.MELEE;
    }
    if (this.baseAttack !== MELEE) {
        return;
    }
    return champions.attackTypes.MELEE;
}

function magic() {
    if (this.toString() === MAGIC) {
        return champions.attackTypes.MAGIC
    }
    if (this.baseAttack !== MAGIC) {
        return;
    }
    return champions.attackTypes.MAGIC
}

function range() {
    if (this.toString() === RANGE) {
        return champions.attackTypes.RANGE
    }
    if (this.baseAttack !== RANGE) {
        return;
    }
    return champions.attackTypes.RANGE
}

function dps() {
    if (this.toString() !== DPS) {
        return;
    }
    return champions.roles[0];
}

function support() {
    if (this.toString() !== SUPPORT) {
        return;
    }
    return champions.roles[1];
}

function tank() {
    if (this.toString() !== TANK) {
        return;
    }
    return champions.roles[2];
}

function healing() {
    if (this.toString() !== HEALING) {
        return;
    }
    return champions.roles[3];
}

function gold() {
    if (this.toString() !== GOLD) {
        return;
    }
    return champions.roles[4];
}

function speed() {
    if (this.toString() !== SPEED) {
        return;
    }
    return champions.roles[5];
}

function debuff() {
    if (this.toString() !== DEBUFF) {
        return;
    }
    return champions.roles[6];
}

const NONE = '';
const ABSOLUTE_ADVERSARIES = 'Absolute Adv';
const ACQ_INC = 'Acq Inc';
const AEROIS = 'Aerois';
const AWFUL_ONES = 'Awful Ones';
const BALDURS_GATE = "Baldur's Gate";
const BLACK_DICE_SOCIETY = 'Black Dice';
const BRIMSTONE = 'Brimstone';
const COMPANIONS = 'Companions';
const C_TEAM = 'C-Team';
const DARK_ORDER = 'Dark Order';
const FORCE_GREY = 'Force Grey';
const HOT_PLANES = 'HotPlanes';
const OXVENTURERS = 'Oxventurers';
const RIVALS = 'Rivals';
const SATURDAY_MORNING_SQUAD = 'Saturday';
const SIRENS = 'Sirens';
const WAFFLE_CREW = 'Waffle Crew';

const LAWFUL_GOOD = 'Lawful Good';
const LAWFUL_NEUTRAL = 'Lawful Neutral';
const LAWFUL_EVIL = 'Lawful Evil';
const NEUTRAL_GOOD = 'Neutral Good';
const NEUTRAL = 'Neutral';
const NEUTRAL_EVIL = 'Neutral Evil';
const CHAOTIC_GOOD = 'Chaotic Good';
const CHAOTIC_NEUTRAL = 'Chaotic Neutral';
const CHAOTIC_EVIL = 'Chaotic Evil';

const bruenor = {
    name: 'Bruenor', baseAttack: MELEE, benchSlot: 1, affiliation: COMPANIONS, alignment: NEUTRAL_GOOD, race: 'Dwarf (Shield)', class: 'Fighter', roles: [SUPPORT], portrait: 'Portraits__Portrait_Bruenor.png',
};

const celeste = {
    name: 'Celeste', baseAttack: MAGIC, benchSlot: 2, affiliation: NONE, alignment: LAWFUL_GOOD, race: 'Human', class: 'Cleric', roles: [HEALING, SUPPORT], portrait: 'Portraits__Portrait_Celeste.png',
};

const nayeli = {
    name: 'Nayeli', baseAttack: MELEE, benchSlot: 3, affiliation: NONE, alignment: LAWFUL_GOOD, race: 'Human', class: 'Paladin', roles: [TANK, SUPPORT], portrait: 'Portraits__Portrait_Nayeli.png',
};

const jarlaxle = {
    name: 'Jarlaxle', baseAttack: MELEE, benchSlot: 4, affiliation: NONE, alignment: NEUTRAL_EVIL, race: 'Elf (Drow)', class: 'Rogue', roles: [DPS, GOLD], portrait: 'Portraits__Portrait_Jarlaxle.png',
};

const calliope = {
    name: 'Calliope', baseAttack: MAGIC, benchSlot: 5, affiliation: FORCE_GREY, alignment: CHAOTIC_GOOD, race: 'Half-Elf', class: 'Bard', roles: [HEALING, SUPPORT], portrait: 'Portraits__Portrait_Calliope.png',
};

const asharra = {
    name: 'Asharra', baseAttack: MAGIC, benchSlot: 6, affiliation: NONE, alignment: LAWFUL_NEUTRAL, race: 'Aarakocra', class: 'Wizard', roles: [DPS, SUPPORT], portrait: 'Portraits__Portrait_Asharra.png',
};

const minsc = {
    name: 'Minsc', baseAttack: MELEE, benchSlot: 7, affiliation: BALDURS_GATE, alignment: CHAOTIC_GOOD, race: 'Human', class: 'Ranger', roles: [DPS, SUPPORT, SPEED, DEBUFF], portrait: 'Portraits__Portrait_Minsc.png',
};

const delina = {
    name: 'Delina', baseAttack: MAGIC, benchSlot: 8, affiliation: BALDURS_GATE, alignment: CHAOTIC_GOOD, race: 'Elf (High)', class: 'Sorcerer', roles: [DPS], portrait: 'Portraits__Portrait_Delina.png',
};

const makos = {
    name: 'Makos', baseAttack: MAGIC, benchSlot: 9, affiliation: NONE, alignment: NEUTRAL_EVIL, race: 'Tiefling', class: 'Warlock', roles: [DPS, SUPPORT, GOLD], portrait: 'Portraits__Portrait_Makos.png',
};

const tyril = {
    name: 'Tyril', baseAttack: MELEE, benchSlot: 10, affiliation: FORCE_GREY, alignment: NEUTRAL_GOOD, race: 'Firbolg', class: 'Druid', roles: [TANK, HEALING, SUPPORT], portrait: 'Portraits__Portrait_Tyril.png',
};

const jamilah = {
    name: 'Jamilah', baseAttack: MELEE, benchSlot: 11, affiliation: FORCE_GREY, alignment: NEUTRAL, race: 'Human', class: 'Barbarian', roles: [DPS], portrait: 'Portraits__Portrait_Jamilah.png',
};

const arkhan = {
    name: 'Arkhan', baseAttack: MELEE, benchSlot: 12, affiliation: [{ row: 1, affiliation: FORCE_GREY }, { row:2, affiliation: DARK_ORDER }], alignment: NEUTRAL_EVIL, race: 'Dragonborn', class: 'Paladin', roles: [DPS, TANK], portrait: 'Portraits__Portrait_Arkhan.png',
};

const azaka = {
    name: 'Azaka', baseAttack: MELEE, benchSlot: 12, affiliation: NONE, alignment: LAWFUL_NEUTRAL, race: 'Human', class: 'Fighter', roles: [DPS, SUPPORT, GOLD], portrait: 'Portraits__Portrait_Azaka.png',
    zone: {
        name: 'Tomb of Annihilation', image: '../img/zones/Icons_Adventures__Icon_AdventureLogo_Tomb.png',
        htmlText: "Complete the adventure variant <span class='golden'>Azaka's Procession Part Two</span>."
    }
};

const dragonbait = {
    name: 'Dragonbait', baseAttack: MELEE, benchSlot: 11, affiliation: NONE, alignment: LAWFUL_GOOD, race: 'Saurial', class: 'Paladin', roles: [TANK, SUPPORT], portrait: 'Portraits__Portrait_DragonbaitHero.png',
    zone: {
        name: 'Tomb of Annihilation', image: '../img/zones/Icons_Adventures__Icon_AdventureLogo_Tomb.png',
        htmlText: "Complete the adventure variant <span class='golden'>A Saurial's Resolve</span>."
    }
};

const drizzt = {
    name: 'Drizzt', baseAttack: MELEE, benchSlot: 9, affiliation: COMPANIONS, alignment: LAWFUL_GOOD, race: 'Elf (Drow)', class: 'Ranger', roles: [DPS], portrait: 'Portraits__Portrait_Drizzit.png',
    zone: {
        name: 'A Grand Tour of the Sword Coast', image: '../img/zones/Icons_Adventures__Icon_AdventureLogo_GrandTour.png',
        htmlText: "Complete the adventure variant <span class='golden'>Overdue Rendezvous</span>."
    }
};

const hitch = {
    name: 'Hitch', baseAttack: [{row: 1, baseAttack: RANGE}, {row: 2, baseAttack: MELEE}], benchSlot: 8, affiliation: FORCE_GREY, alignment: CHAOTIC_GOOD, race: 'Human', class: 'Rogue', roles: [SUPPORT], portrait: 'Portraits__Portrait_Hitch.png',
    zone: {
        name: 'Newsletter', image: '../img/evergreen/newsletter.png',
        htmlText: "<span class='golden'>Sign up</span> for the <span class='golden'>newsletter</span> through the <span class='golden'>in-game menu</span>, then wait for a <span class='golden'>code</span> to arrive in your <span class='golden'>email</span>."
    }
};

const nerds = {
    name: 'Nerds', baseAttack: [{row: 1, baseAttack: MELEE}, {row: 2, baseAttack: RANGE}, {row: 3, baseAttack: MAGIC}], benchSlot: 1, affiliation: NONE, alignment: CHAOTIC_NEUTRAL, race: 'NERDS', class: 'Fighter / Ranger / Bard / Cleric / Rogue / Wizard', roles: [DPS, TANK, HEALING, SUPPORT, DEBUFF], portrait: 'Portraits__Portrait_Nerds.png',
    zone: {
        name: 'Nerds', image: '../img/evergreen/nerds.png',
        htmlText: "Initialy available from <a href='https://www.nerdscandypromotion.com/' target='_blank'>nerdscandypromotion.com</a><br>Now it's available in the <span class='golden'>gem shop</span> for 100,000 gems."
    }
};

const reya = {
    name: 'Reya', baseAttack: MELEE, benchSlot: 6, affiliation: NONE, alignment: LAWFUL_GOOD, race: 'Human / Celestial', class: 'Paladin', roles: [SUPPORT], portrait: 'Portraits__Portrait_Reya.png',
    zone: {
        name: "Baldur's Gate Descent into Avernus", image: '../img/zones/Icons_Adventures__Icon_AdventureLogo_Avernus.png',
        htmlText: "Complete the adventure variant <span class='golden'>Reya Reborn</span>."
    }
};

const ulkoria = {
    name: 'Ulkoria', baseAttack: MAGIC, benchSlot: 10, affiliation: NONE, alignment: NEUTRAL_GOOD, race: 'Dwarf', class: 'Wizard', roles: [SUPPORT], portrait: 'Portraits__Portrait_Ulkoria.png',
    zone: {
        name: 'Waterdeep Dragon Heist', image: '../img/zones/Icons_Adventures__Icon_AdventureLogo_DragonHeist.png',
        htmlText: "Complete the adventure variant <span class='golden'>The Gargoyle</span>."
    }
};

const xerophon = {
    name: 'Xerophon', baseAttack: MELEE, benchSlot: 7, affiliation: NONE, alignment: NEUTRAL, race: 'Doppelganger', class: 'Rogue', roles: [SUPPORT], portrait: 'Portraits__Portrait_Xerophon.png',
    zone: {
        name: 'Icewind Dale Rime of the Frostmaiden', image: '../img/zones/Icons_Adventures__Icon_AdventureLogo_IcewindDale.png',
        htmlText: "Complete the adventure variant <span class='golden'>A Lesson in Classes</span>."
    }
};

const aila = {
    name: 'Aila', baseAttack: MELEE, benchSlot: 9, affiliation: AEROIS, alignment: CHAOTIC_NEUTRAL, race: 'Elf (Wild)', class: 'Barbarian', roles: [TANK, SUPPORT, DEBUFF], portrait: 'Portraits__Portrait_Aila.png', isTGOnly: true,
};

const alyndra = {
    name: 'Alyndra', baseAttack: MAGIC, benchSlot: 6, affiliation: HOT_PLANES, alignment: NEUTRAL, race: 'Elf (High)', class: 'Wizard / Cleric', roles: [SUPPORT], portrait: 'Portraits__Portrait_Alyndra.png', isTGOnly: true,
};

const artemis = {
    name: 'Artemis', baseAttack: MELEE, benchSlot: 3, affiliation: NONE, alignment: LAWFUL_EVIL, race: 'Human', class: 'Rogue', roles: [DPS], portrait: 'Portraits__Portrait_Artemis.png', isTGOnly: true,
};

const avren = {
    name: 'Avren', baseAttack: MAGIC, benchSlot: 11, affiliation: HOT_PLANES, alignment: CHAOTIC_NEUTRAL, race: 'Half-Elf', class: 'Warlock / Sorcerer', roles: [SUPPORT], portrait: 'Portraits__Portrait_Avren.png', isTGOnly: true,
};

const baeloth = {
    name: 'Baeloth', baseAttack: MAGIC, benchSlot: 4, affiliation: BALDURS_GATE, alignment: CHAOTIC_EVIL, race: 'Elf (Drow)', class: 'Sorcerer', roles: [SUPPORT, GOLD], portrait: 'Portraits__Portrait_Baeloth.png', isTGOnly: true,
};

const barrowin = {
    name: 'Barrowin', baseAttack: MELEE, benchSlot: 10, affiliation: NONE, alignment: LAWFUL_GOOD, race: 'Dwarf', class: 'Cleric', roles: [HEALING, SUPPORT], portrait: 'Portraits__Portrait_Barrowin.png', isTGOnly: true,
};

const beadle_grimm = {
    name: 'Beadle & Grimm', isBeadleAndGrimm: true, baseAttack: { b: { baseAttack: MELEE }, g: { baseAttack: MELEE }}, benchSlot: 8, affiliation: NONE, alignment: { b: 'Neutral', g: 'Chaotic Good'}, race: { b: 'Dwarf', g: 'Human'}, class: { b: 'Rogue / Wizard', g: 'Barbarian'}, roles: { b: [SUPPORT, GOLD], g: [DPS, SUPPORT]}, portrait: 'beadle_and_grimm.png', isTGOnly: true,
};

const binwin = {
    name: 'Binwin', baseAttack: MELEE, benchSlot: 3, affiliation: NONE, alignment: LAWFUL_GOOD, race: 'Dwarf (Mountain)', class: 'Fighter / Barbarian', roles: [DPS], portrait: 'Portraits__Portrait_Binwin.png', isTGOnly: true
};

const birdsong = {
    name: 'Birdsong', baseAttack: MELEE, benchSlot: 9, affiliation: NONE, alignment: LAWFUL_NEUTRAL, race: 'Tabaxi', class: 'Bard', roles: [DPS, SUPPORT], portrait: 'Portraits__Portrait_Birdsong.png', isTGOnly: true,
};

const blackviper = {
    name: 'Black Viper', baseAttack: MELEE, benchSlot: 7, affiliation: NONE, alignment: CHAOTIC_NEUTRAL, race: 'Human', class: 'Rogue', roles: [DPS], portrait: 'Portraits__Portrait_BlackViper.png', isTGOnly: true,
};

const brig = {
    name: 'Brig', baseAttack: MELEE, benchSlot: 3, affiliation: SIRENS, alignment: CHAOTIC_GOOD, race: 'Human', class: 'Rogue / Bard', roles: [SUPPORT], portrait: 'Portraits__Portrait_BrigHellclaw.png', isTGOnly: true,
};

const briv = {
    name: 'Briv', baseAttack: MELEE, benchSlot: 5, affiliation: HOT_PLANES, alignment: CHAOTIC_GOOD, race: 'Half-Orc', class: 'Paladin', roles: [TANK, HEALING, SUPPORT, SPEED], portrait: 'Portraits__Portrait_Briv.png', isTGOnly: true,
};

const cattibrie = {
    name: 'Catti-brie', baseAttack: RANGE, benchSlot: 7, affiliation: COMPANIONS, alignment: CHAOTIC_GOOD, race: 'Human', class: 'Fighter', roles: [DPS, SUPPORT, DEBUFF], portrait: 'Portraits__Portrait_CattiBrie.png', isTGOnly: true,
};

const corazon = {
    name: 'Corazón', baseAttack: MELEE, benchSlot: 8, affiliation: OXVENTURERS, alignment: CHAOTIC_NEUTRAL, race: 'Human', class: 'Rogue', roles: [SUPPORT], portrait: 'Portraits__Portrait_Corazon.png', isTGOnly: true,
};

const dhani = {
    name: "D'hani", baseAttack: MELEE, benchSlot: 1, affiliation: RIVALS, alignment: CHAOTIC_GOOD, race: 'Aarakocra', class: 'Kensei Monk', roles: [DPS, GOLD], portrait: 'Portraits__Portrait_Dhani.png', isTGOnly: true,
};

const deekin = {
    name: 'Deekin', baseAttack: RANGE, benchSlot: 1, affiliation: NONE, alignment: NEUTRAL, race: 'Kobold', class: 'Bard', roles: [SUPPORT, SPEED], portrait: 'Portraits__Portrait_Deekin.png', isTGOnly: true,
};

const dhadius = {
    name: 'Dhadius', baseAttack: MAGIC, benchSlot: 5, affiliation: NONE, alignment: CHAOTIC_NEUTRAL, race: 'Human', class: 'Wizard', roles: [DPS, SUPPORT], portrait: 'Portraits__Portrait_Dhadius.png', isTGOnly: true,
};

const donaar = {
    name: 'Donaar', baseAttack: MAGIC, benchSlot: 2, affiliation: C_TEAM, alignment: CHAOTIC_GOOD, race: 'Dragonborn', class: 'Paladin', roles: [HEALING, SUPPORT, GOLD, DEBUFF], portrait: 'Portraits__Portrait_Donaar.png', isTGOnly: true,
};

const ellywick = {
    name: 'Ellywick', baseAttack: MAGIC, benchSlot: 10, affiliation: NONE, alignment: CHAOTIC_NEUTRAL, race: 'Gnome', class: 'Bard', roles: [HEALING, SUPPORT, GOLD], portrait: 'Portraits__Portrait_Ellywick.png', isTGOnly: true,
};

const evelyn = {
    name: 'Evelyn', baseAttack: MELEE, benchSlot: 6, affiliation: [{ row: 1, affiliation: WAFFLE_CREW }, { row:2, affiliation: ACQ_INC }], alignment: NEUTRAL_GOOD, race: 'Human', class: 'Paladin', roles: [TANK, SUPPORT], portrait: 'Portraits__Portrait_Evelyn.png', isTGOnly: true,
};

const ezmerelda = {
    name: 'Ezmerelda', baseAttack: MAGIC, benchSlot: 1, affiliation: NONE, alignment: CHAOTIC_GOOD, race: 'Human', class: 'Fighter / Wizard', roles: [SUPPORT], portrait: 'Portraits__Portrait_Ezmerelda.png', isTGOnly: true,
};

const farideh = {
    name: 'Farideh', baseAttack: MAGIC, benchSlot: 7, affiliation: BRIMSTONE, alignment: NEUTRAL_GOOD, race: 'Tiefling', class: 'Warlock', roles: [DPS], portrait: 'Portraits__Portrait_Farideh.png', isTGOnly: true,
};

const freely = {
    name: 'Freely', baseAttack: MAGIC, benchSlot: 7, affiliation: HOT_PLANES, alignment: NEUTRAL_GOOD, race: 'Halfling', class: 'Bard / Warlock', roles: [SUPPORT, GOLD, DEBUFF], portrait: 'Portraits__Portrait_Freely.png', isTGOnly: true,
};

const gromma = {
    name: 'Gromma', baseAttack: [{row: 1, baseAttack: MELEE}, {row: 2, baseAttack: MAGIC}], benchSlot: 3, affiliation: NONE, alignment: LAWFUL_NEUTRAL, race: 'Tortle', class: 'Druid', roles: [DPS, TANK, SUPPORT, DEBUFF], portrait: 'Portraits__Portrait_Gromma.png', isTGOnly: true,
};

const havilar = {
    name: 'Havilar', baseAttack: MELEE, benchSlot: 10, affiliation: BRIMSTONE, alignment: CHAOTIC_GOOD, race: 'Tiefling', class: 'Fighter', roles: [TANK, SUPPORT, SPEED], portrait: 'Portraits__Portrait_Havilar.png', isTGOnly: true,
};

const hewmaan = {
    name: 'Hew Maan', baseAttack: RANGE, benchSlot: 8, affiliation: NONE, alignment: LAWFUL_EVIL, race: 'Kobold / "Human"', class: 'Rogue / Bard', roles: [SUPPORT, SPEED], portrait: 'Portraits__Portrait_HewMaan.png', isTGOnly: true,
};

const ishi = {
    name: 'Ishi', baseAttack: MELEE, benchSlot: 4, affiliation: NONE, alignment: CHAOTIC_GOOD, race: 'Kobold', class: 'Fighter / Rogue', roles: [DPS, GOLD], portrait: 'Portraits__Portrait_Ishi.png', isTGOnly: true,
};

const jaheira = {
    name: 'Jaheira', baseAttack: MELEE, benchSlot: 9, affiliation: BALDURS_GATE, alignment: NEUTRAL, race: 'Half-Elf', class: 'Fighter / Druid', roles: [DPS, SUPPORT], portrait: 'Portraits__Portrait_Jaheira.png', isTGOnly: true, isVajra: true,
};

const jim = {
    name: 'Jim', baseAttack: MAGIC, benchSlot: 7, affiliation: ACQ_INC, alignment: CHAOTIC_NEUTRAL, race: 'Human', class: 'Wizard', roles: [DPS, SUPPORT, GOLD], portrait: 'Portraits__Portrait_JimDarkmagic.png', isTGOnly: true,
};

const korth = {
    name: 'Korth', baseAttack: MELEE, benchSlot: 2, affiliation: NONE, alignment: NEUTRAL_EVIL, race: 'Lizardfolk', class: 'Samurai (Fighter)', roles: [HEALING, SUPPORT, DEBUFF], portrait: 'Portraits__Portrait_Korth.png', isTGOnly: true,
};

const krond = {
    name: 'Krond', baseAttack: [{row: 1, baseAttack: MAGIC}, {row: 2, baseAttack: MELEE}], benchSlot: 6, affiliation: NONE, alignment: CHAOTIC_EVIL, race: 'Half-Orc', class: 'Fighter', roles: [DPS, DEBUFF], portrait: 'Portraits__Portrait_Krond.png', isTGOnly: true, isStrahd: true,
};

const krull = {
    name: 'Krull', baseAttack: MELEE, benchSlot: 6, affiliation: DARK_ORDER, alignment: LAWFUL_EVIL, race: 'Tortle', class: 'Cleric', roles: [SUPPORT, GOLD, DEBUFF], portrait: 'Portraits__Portrait_Krull.png', isTGOnly: true,
};

const krydle = {
    name: 'Krydle', baseAttack: MELEE, benchSlot: 2, affiliation: BALDURS_GATE, alignment: CHAOTIC_GOOD, race: 'Half-Elf', class: 'Rogue', roles: [TANK, SUPPORT], portrait: 'Portraits__Portrait_Krydle.png', isTGOnly: true,
};

const kthriss = {
    name: "K'thriss", baseAttack: MAGIC, benchSlot: 1, affiliation: C_TEAM, alignment: CHAOTIC_NEUTRAL, race: 'Elf (Drow)', class: 'Warlock', roles: [SUPPORT, DEBUFF], portrait: 'Portraits__Portrait_Kthriss.png', isTGOnly: true,
};

const lazaapz = {
    name: 'Lazaapz', baseAttack: MELEE, benchSlot: 9, affiliation: NONE, alignment: NEUTRAL_EVIL, race: 'Goblin', class: 'Artificer', roles: [TANK, SUPPORT], portrait: 'Portraits__Portrait_Lazaapz.png', isTGOnly: true,
};

const lucius = {
    name: 'Lucius', baseAttack: MAGIC, benchSlot: 7, affiliation: AEROIS, alignment: LAWFUL_NEUTRAL, race: 'Elf (High)', class: 'Sorcerer', roles: [DPS, DEBUFF], portrait: 'Portraits__Portrait_Lucius.png', isTGOnly: true,
};

const mehen = {
    name: 'Mehen', baseAttack: MELEE, benchSlot: 3, affiliation: BRIMSTONE, alignment: NEUTRAL_GOOD, race: 'Dragonborn', class: 'Fighter', roles: [SUPPORT, GOLD], portrait: 'Portraits__Portrait_Mehen.png', isTGOnly: true,
};

const melf = {
    name: 'Melf', baseAttack: MELEE, benchSlot: 12, affiliation: NONE, alignment: NEUTRAL_GOOD, race: 'Elf (High)', class: 'Fighter / Wizard', roles: [SUPPORT, SPEED], portrait: 'Portraits__Portrait_Melf.png', isTGOnly: true,
};

const morgaen = {
    name: 'Môrgæn', baseAttack: RANGE, benchSlot: 9, affiliation: ACQ_INC, alignment: CHAOTIC_NEUTRAL, race: 'Elf (High)', class: 'Ranger', roles: [SUPPORT, GOLD], portrait: 'Portraits__Portrait_Morgaen.png', isTGOnly: true,
};

const nerys = {
    name: 'Nerys', baseAttack: MELEE, benchSlot: 12, affiliation: BALDURS_GATE, alignment: LAWFUL_GOOD, race: 'Human', class: 'Cleric', roles: [HEALING, SUPPORT, DEBUFF], portrait: 'Portraits__Portrait_Nerys.png', isTGOnly: true,
};

const nova = {
    name: 'Nova', baseAttack: MELEE, benchSlot: 11, affiliation: AEROIS, alignment: CHAOTIC_GOOD, race: 'Air Genasi', class: 'Warlock', roles: [TANK, SUPPORT], portrait: 'Portraits__Portrait_Nova.png', isTGOnly: true,
};

const nrakk = {
    name: 'Nrakk', baseAttack: MELEE, benchSlot: 8, affiliation: NONE, alignment: LAWFUL_NEUTRAL, race: 'Githzerai', class: 'Monk', roles: [SUPPORT, DEBUFF], portrait: 'Portraits__Portrait_Nrakk.png', isTGOnly: true,
};

const omin = {
    name: 'Omin', baseAttack: MELEE, benchSlot: 3, affiliation: ACQ_INC, alignment: LAWFUL_NEUTRAL, race: 'Half-Elf', class: 'Cleric', roles: [HEALING, SUPPORT, GOLD], portrait: 'Portraits__Portrait_Omin.png', isTGOnly: true,
};

const orisha = {
    name: 'Orisha', baseAttack: MAGIC, benchSlot: 11, affiliation: SIRENS, alignment: NEUTRAL_GOOD, race: 'Aasimar', class: 'Cleric / Bard', roles: [DPS, SUPPORT], portrait: 'Portraits__Portrait_Orisha.png', isTGOnly: true,
};

const orkira = {
    name: 'Orkira', baseAttack: MAGIC, benchSlot: 1, affiliation: HOT_PLANES, alignment: CHAOTIC_GOOD, race: 'Dragonborn', class: 'Cleric', roles: [HEALING, SUPPORT, DEBUFF], portrait: 'Portraits__Portrait_Orkira.png', isTGOnly: true,
};

const paultin = {
    name: 'Paultin', baseAttack: RANGE, benchSlot: 4, affiliation: WAFFLE_CREW, alignment: CHAOTIC_NEUTRAL, race: 'Human', class: 'Bard', roles: [SUPPORT, GOLD], portrait: 'Portraits__Portrait_Paultin.png', isTGOnly: true,
};

const penelope = {
    name: 'Penelope', baseAttack: MELEE, benchSlot: 12, affiliation: HOT_PLANES, alignment: CHAOTIC_GOOD, race: 'Halfling', class: 'Druid / Warlock', roles: [HEALING, SUPPORT, GOLD, DEBUFF], portrait: 'Portraits__Portrait_Penelope.png', isTGOnly: true,
};

const prudence = {
    name: 'Prudence', baseAttack: MAGIC, benchSlot: 5, affiliation: OXVENTURERS, alignment: CHAOTIC_EVIL, race: 'Tiefling', class: 'Warlock', roles: [DPS], portrait: 'Portraits__Portrait_Prudence.png', isTGOnly: true,
};

const pwent = {
    name: 'Pwent', baseAttack: MELEE, benchSlot: 5, affiliation: NONE, alignment: CHAOTIC_GOOD, race: 'Dwarf', class: 'Barbarian', roles: [SUPPORT], portrait: 'Portraits__Portrait_Thibbledorf.png', isTGOnly: true,
};

const qillek = {
    name: 'Qillek', baseAttack: MAGIC, benchSlot: 5, affiliation: AEROIS, alignment: LAWFUL_GOOD, race: 'Aarakocra', class: 'Cleric / Bard', roles: [HEALING, SUPPORT], portrait: 'Portraits__Portrait_Qillek.png', isTGOnly: true,
};

const regis = {
    name: 'Regis', baseAttack: MELEE, benchSlot: 2, affiliation: COMPANIONS, alignment: CHAOTIC_NEUTRAL, race: 'Halfling', class: 'Rogue', roles: [HEALING, SUPPORT, GOLD, DEBUFF], portrait: 'Portraits__Portrait_Regis.png', isTGOnly: true, isStrahd: true,
};

const rosie = {
    name: 'Rosie', baseAttack: MELEE, benchSlot: 10, affiliation: C_TEAM, alignment: CHAOTIC_GOOD, race: 'Halfling', class: 'Monk', roles: [DPS], portrait: 'Portraits__Portrait_Rosie.png', isTGOnly: true,
};

const selise = {
    name: 'Selise', baseAttack: MELEE, benchSlot: 12, affiliation: RIVALS, alignment: LAWFUL_NEUTRAL, race: 'Human', class: 'Paladin', roles: [TANK, SUPPORT], portrait: 'Portraits__Portrait_Selise.png', isTGOnly: true,
};

const sentry = {
    name: 'Sentry', baseAttack: MELEE, benchSlot: 4, affiliation: AEROIS, alignment: LAWFUL_GOOD, race: 'Warforged', class: 'Paladin', roles: [TANK, SUPPORT, SPEED], portrait: 'Portraits__Portrait_Sentry.png', isTGOnly: true,
};

const sgtknox = {
    name: 'Sgt. Knox', baseAttack: MELEE, benchSlot: 6, affiliation: NONE, alignment: NEUTRAL_GOOD, race: 'Human', class: 'Fighter', roles: [TANK, SUPPORT], portrait: 'Portraits__Portrait_SgtKnox.png', isTGOnly: true,
};

const shaka = {
    name: 'Shaka', baseAttack: MAGIC, benchSlot: 9, affiliation: RIVALS, alignment: NEUTRAL_GOOD, race: 'Tiefling', class: 'Warlock', roles: [HEALING, SUPPORT], portrait: 'Portraits__Portrait_Shaka.png', isTGOnly: true,
};

const shandie = {
    name: 'Shandie', baseAttack: RANGE, benchSlot: 6, affiliation: BALDURS_GATE, alignment: CHAOTIC_GOOD, race: 'Halfling', class: 'Rogue', roles: [SUPPORT, SPEED], portrait: 'Portraits__Portrait_Shandie.png', isTGOnly: true,
};

const sisaspia = {
    name: 'Sisaspia', baseAttack: MAGIC, benchSlot: 1, affiliation: NONE, alignment: NEUTRAL_EVIL, race: 'Yuan-ti Pureblood', class: 'Druid', roles: [HEALING, SUPPORT], portrait: 'Portraits__Portrait_Sisaspia.png', isTGOnly: true,
};

const spurt = {
    name: 'Spurt', baseAttack: RANGE, benchSlot: 3, affiliation: NONE, alignment: LAWFUL_EVIL, race: 'Kobold', class: 'Inventor (Rogue)', roles: [SUPPORT, DEBUFF], portrait: 'Portraits__Portrait_Spurt.png', isTGOnly: true,
};

const stoki = {
    name: 'Stoki', baseAttack: MELEE, benchSlot: 4, affiliation: NONE, alignment: LAWFUL_NEUTRAL, race: 'Gnome', class: 'Monk', roles: [SUPPORT, GOLD, DEBUFF], portrait: 'Portraits__Portrait_Pickle.png', isTGOnly: true,
};

const strix = {
    name: 'Strix', baseAttack: MAGIC, benchSlot: 11, affiliation: [{ row: 1, affiliation: WAFFLE_CREW }, { row:2, affiliation: ACQ_INC }], alignment: CHAOTIC_GOOD, race: 'Tiefling', class: 'Sorcerer', roles: [DPS, SUPPORT, DEBUFF], portrait: 'Portraits__Portrait_Strix.png', isTGOnly: true,
};

const talin = {
    name: 'Talin', baseAttack: MELEE, benchSlot: 2, affiliation: NONE, alignment: CHAOTIC_GOOD, race: 'Tiefling', class: 'Rogue', roles: [SUPPORT, DEBUFF], portrait: 'Portraits__Portrait_TalinUran.png', isTGOnly: true,
};

const torogar = {
    name: 'Torogar', baseAttack: MELEE, benchSlot: 10, affiliation: DARK_ORDER, alignment: LAWFUL_EVIL, race: 'Minotaur', class: 'Barbarian', roles: [DPS, SUPPORT], portrait: 'Portraits__Portrait_Torogar.png', isTGOnly: true,
};

const turiel = {
    name: 'Turiel', baseAttack: MAGIC, benchSlot: 1, affiliation: NONE, alignment: LAWFUL_GOOD, race: 'Aasimar', class: 'Cleric', roles: [SUPPORT], portrait: 'Portraits__Portrait_Turiel.png', isTGOnly: true,
};

const viconia = {
    name: 'Viconia', baseAttack: RANGE, benchSlot: 5, affiliation: BALDURS_GATE, alignment: NEUTRAL_EVIL, race: 'Elf (Drow)', class: 'Cleric', roles: [HEALING, SUPPORT], portrait: 'Portraits__Portrait_Viconia.png', isTGOnly: true,
};

const vlahnya = {
    name: 'Vlahnya', baseAttack: MAGIC, benchSlot: 8, affiliation: SIRENS, alignment: NEUTRAL_GOOD, race: 'Eladrin', class: 'Bard / Cleric', roles: [SUPPORT], portrait: 'Portraits__Portrait_Vlahnya.png', isTGOnly: true,
};

const walnut = {
    name: 'Walnut', baseAttack: MELEE, benchSlot: 8, affiliation: C_TEAM, alignment: LAWFUL_NEUTRAL, race: 'Elf (Wood)', class: 'Druid', roles: [TANK, SUPPORT], portrait: 'Portraits__Portrait_Walnut.png', isTGOnly: true,
};

const warden = {
    name: 'Warden', baseAttack: MELEE, benchSlot: 11, affiliation: NONE, alignment: CHAOTIC_EVIL, race: 'Warforged (Skirmisher)', class: 'Warlock (Hexblade)', roles: [DPS, SUPPORT, DEBUFF], portrait: 'Portraits__Portrait_Warden.png', isTGOnly: true, isMirt: true,
};

const widdle = {
    name: 'Widdle', baseAttack: MAGIC, benchSlot: 2, affiliation: HOT_PLANES, alignment: CHAOTIC_NEUTRAL, race: 'Dhampir Gnome', class: 'Wizard', roles: [HEALING, SUPPORT, SPEED], portrait: 'Portraits__Portrait_Widdle.png', isTGOnly: true,
};

const wulfgar = {
    name: 'Wulfgar', baseAttack: MELEE, benchSlot: 10, affiliation: COMPANIONS, alignment: CHAOTIC_GOOD, race: 'Human', class: 'Barbarian', roles: [TANK, SUPPORT], portrait: 'Portraits__Portrait_Wulfgar.png', isTGOnly: true,
};

const xander = {
    name: 'Xander', baseAttack: MELEE, benchSlot: 5, affiliation: NONE, alignment: CHAOTIC_GOOD, race: 'Human', class: 'Rogue', roles: [SUPPORT, SPEED], portrait: 'Portraits__Portrait_Xander.png', isTGOnly: true,
};

const yorven = {
    name: 'Yorven', baseAttack: MELEE, benchSlot: 10, affiliation: NONE, alignment: CHAOTIC_NEUTRAL, race: 'Harengon', class: 'Barbarian / Druid', roles: [DPS, HEALING, SUPPORT, DEBUFF], portrait: 'Portraits__Portrait_YorvenSpringpaw.png', isTGOnly: true,
};

const zorbu = {
    name: 'Zorbu', baseAttack: RANGE, benchSlot: 12, affiliation: NONE, alignment: CHAOTIC_GOOD, race: 'Gnome', class: 'Ranger', roles: [DPS, SUPPORT], portrait: 'Portraits__Portrait_Zorbo.png', isTGOnly: true,
};

const blooshi = {
    name: 'Blooshi', baseAttack: MAGIC, benchSlot: 2, affiliation: NONE, alignment: NEUTRAL_EVIL, race: 'Bullywug', class: 'Warlock', roles: [TANK, SUPPORT], portrait: 'Portraits__Portrait_Blooshi.png',
    zone: {
        name: 'The Wild Beyond the Witchlight', image: '../img/zones/Icons_Adventures__Icon_AdventureLogo_Witchlight.png',
        htmlText: "Complete the adventure variant <span class='golden'>Bully For You!</span>."
    }
};

const rust = {
    name: 'Rust', baseAttack: [{row: 1, baseAttack: MELEE}, {row: 2, baseAttack: RANGE}], benchSlot: 11, affiliation: OXVENTURERS, alignment: NEUTRAL, race: 'Tabaxi', class: 'Rogue', roles: [SUPPORT, GOLD], portrait: 'Portraits__Portrait_Rust.png', isTGOnly: true,
};

const vi = {
    name: 'Vi', baseAttack: RANGE, benchSlot: 12, affiliation: ACQ_INC, alignment: NEUTRAL_GOOD, race: 'Gnome', class: 'Artificer', roles: [SUPPORT, GOLD, SPEED], portrait: 'Portraits__Portrait_Vi.png', isTGOnly: true,
};

const desmond = {
    name: 'Desmond', baseAttack: [{row: 1, baseAttack: MELEE}, {row: 2, baseAttack: RANGE}], benchSlot: 4, affiliation: BLACK_DICE_SOCIETY, alignment: CHAOTIC_GOOD, race: 'Human', class: 'Ranger', roles: [SUPPORT], portrait: 'Portraits__Portrait_Desmond.png',
};

const tatyana = {
    name: 'Tatyana', baseAttack: MELEE, benchSlot: 8, affiliation: BLACK_DICE_SOCIETY, alignment: CHAOTIC_NEUTRAL, race: 'Air Genasi', class: 'Barbarian / Druid', roles: [TANK, SUPPORT, SPEED], portrait: 'Portraits__Portrait_Tatyana.png',
};

const gazrick = {
    name: 'Gazrick', baseAttack: MAGIC, benchSlot: 7, affiliation: RIVALS, alignment: NEUTRAL_GOOD, race: 'Gnome (Rock)', class: 'Druid', roles: [SUPPORT, GOLD, DEBUFF], portrait: 'Portraits__Portrait_Gazrick.png',
};

const dungeon_master = {
    name: 'Dungeon Master', baseAttack: MAGIC, benchSlot: 6, affiliation: NONE, alignment: LAWFUL_GOOD, race: 'Human', class: 'Wizard', roles: [HEALING, SUPPORT, GOLD], portrait: 'Portraits__Portrait_Rocket.png',
};

const nordom = {
    name: 'Nordom', baseAttack: RANGE, benchSlot: 9, affiliation: NONE, alignment: LAWFUL_NEUTRAL, race: 'Modron', class: 'Fighter', roles: [SUPPORT], portrait: 'Portraits__Portrait_Nordom.png',
};

const merilwen = {
    name: 'Merilwen', baseAttack: MAGIC, benchSlot: 2, affiliation: OXVENTURERS, alignment: NEUTRAL, race: 'Elf (Wood)', class: 'Druid', roles: [SUPPORT, GOLD], portrait: 'Portraits__Portrait_Merilwen.png'
};

const nahara = {
    name: 'Nahara', baseAttack: MAGIC, benchSlot: 3, affiliation: BLACK_DICE_SOCIETY, alignment: CHAOTIC_NEUTRAL, race: 'Aasimar', class: 'Warlock / Bard', roles: [DPS, SPEED], portrait: 'Portraits__Portrait_Nahara.png'
};

const valentine = {
    name: 'Valentine', baseAttack: MAGIC, benchSlot: 5, affiliation: BLACK_DICE_SOCIETY, alignment: NEUTRAL, race: 'Half-Elf (Reborn)', class: 'Sorcerer', roles: [SUPPORT, GOLD], portrait: 'Portraits__Portrait_Valentine.png'
};

const voronika = {
    name: 'Voronika', baseAttack: MAGIC, benchSlot: 1, affiliation: BLACK_DICE_SOCIETY, alignment: CHAOTIC_EVIL, race: 'Eladrin', class: 'Druid', roles: [SUPPORT], portrait: 'Portraits__Portrait_Voronika.png'
};

const dob = {
    name: 'Dob', baseAttack: MAGIC, benchSlot: 12, affiliation: OXVENTURERS, alignment: CHAOTIC_GOOD, race: 'Half-Orc', class: 'Bard', roles: [SUPPORT, GOLD], portrait: 'Portraits__Portrait_Dob.png'
};

const egbert = {
    name: 'Egbert', baseAttack: RANGE, benchSlot: 7, affiliation: OXVENTURERS, alignment: CHAOTIC_GOOD, race: 'Dragonborn', class: 'Paladin', roles: [TANK, HEALING, SUPPORT, GOLD], portrait: 'Portraits__Portrait_Egbert.png'
};

const kent = {
    name: 'Kent', baseAttack: MELEE, benchSlot: 4, affiliation: RIVALS, alignment: NEUTRAL_GOOD, race: 'Tiefling', class: 'Rogue', roles: [DPS, SUPPORT, DEBUFF], portrait: 'Portraits__Portrait_Kent.png'
};

const virgil = {
    name: 'Virgil', baseAttack: MAGIC, benchSlot: 10, affiliation: RIVALS, alignment: NEUTRAL_GOOD, race: 'Aasimar', class: 'Sorcerer', roles: [SUPPORT, SPEED, DEBUFF], portrait: 'Portraits__Portrait_Virgil.png'
};

const warduke = {
    name: 'Warduke', baseAttack: MELEE, benchSlot: 8, affiliation: NONE, alignment: CHAOTIC_EVIL, race: 'Human', class: 'Fighter', roles: [DPS, TANK], portrait: 'Portraits__Portrait_Warduke.png'
};

const imoen = {
    name: 'Imoen', baseAttack: RANGE, benchSlot: 11, affiliation: BALDURS_GATE, alignment: NEUTRAL_GOOD, race: 'Human', class: 'Rogue / Wizard', roles: [HEALING, SUPPORT], portrait: 'Portraits__Portrait_Imoen.png'
};

const fen = {
    name: 'Fen', baseAttack: MELEE, benchSlot: 6, affiliation: BLACK_DICE_SOCIETY, alignment: CHAOTIC_EVIL, race: 'Dhampir Elf (Drow)', class: 'Warlock / Rogue', roles: [DPS, SUPPORT], portrait: 'Portraits__Portrait_Fen.png'
};

const uriah = {
    name: 'Brother Uriah', baseAttack: MAGIC, benchSlot: 9, affiliation: BLACK_DICE_SOCIETY, alignment: LAWFUL_GOOD, race: 'Human', class: 'Cleric', roles: [HEALING, SUPPORT], portrait: 'Portraits__Portrait_Uriah.png'
};

const solaak = {
    name: 'Solaak', baseAttack: RANGE, benchSlot: 2, affiliation: NONE, alignment: NEUTRAL_GOOD, race: 'Kalashtar', class: 'Ranger', roles: [SUPPORT, DEBUFF], portrait: 'Portraits__Portrait_Solaak.png'
};

const miria = {
    name: 'Miria', baseAttack: MAGIC, benchSlot: 12, affiliation: NONE, alignment: LAWFUL_EVIL, race: 'Elf (Shadar-kai)', class: 'Wizard', roles: [TANK, SUPPORT], portrait: 'Portraits__Portrait_Miria.png'
};

const antrius = {
    name: 'Antrius', baseAttack: MAGIC, benchSlot: 4, affiliation: AWFUL_ONES, alignment: CHAOTIC_GOOD, race: 'Human', class: 'Bard', roles: [HEALING, SUPPORT], portrait: 'Portraits__Portrait_Antrius.png'
};

const nixie = {
    name: 'Nixie', baseAttack: MAGIC, benchSlot: 1, affiliation: AWFUL_ONES, alignment: CHAOTIC_NEUTRAL, race: 'Tiefling', class: 'Sorcerer (Wild Magic)', roles: [DPS, SUPPORT], portrait: 'Portraits__Portrait_Nixie.png'
};

const evandra = {
    name: 'Evandra', baseAttack: MELEE, benchSlot: 5, affiliation: AWFUL_ONES, alignment: CHAOTIC_NEUTRAL, race: 'Half-Elf', class: 'Fighter', roles: [TANK, SUPPORT, GOLD], portrait: 'Portraits__Portrait_Evandra.png'
};

const bbeg = {
    name: 'BBEG', baseAttack: MAGIC, benchSlot: 3, affiliation: NONE, alignment: LAWFUL_EVIL, race: 'Elf (Drow)', class: 'Wizard', roles: [SUPPORT, SPEED], portrait: 'Portraits__Portrait_BBEG.png'
};

const strongheart = {
    name: 'Strongheart', baseAttack: MELEE, benchSlot: 11, affiliation: NONE, alignment: LAWFUL_GOOD, race: 'Human', class: 'Paladin', roles: [HEALING, SUPPORT], portrait: 'Portraits__Portrait_Strongheart.png'
};

const krux = {
    name: 'Commodore Krux', baseAttack: RANGE, benchSlot: 4, affiliation: NONE, alignment: LAWFUL_GOOD, race: 'Giff', class: 'Fighter', roles: [DPS, TANK, SUPPORT], portrait: 'Portraits__Portrait_CommodoreKrux.png',
    zone: {
        name: 'The Wild Beyond the Witchlight', image: '../img/zones/Icons_Adventures__Icon_AdventureLogo_LightofXaryxis.png',
        htmlText: "Complete the adventure variant <span class='golden'>Animated Giff</span>."
    }
};

const vinursa = {
    name: 'Vin Ursa', baseAttack: RANGE, benchSlot: 7, affiliation: NONE, alignment: LAWFUL_EVIL, race: 'Plasmoid', class: 'Fighter / Rogue', roles: [SUPPORT, GOLD, DEBUFF], portrait: 'Portraits__Portrait_VinUrsa.png'
};

const laezel = {
    name: 'Lae\'zel', baseAttack: [{row: 1, baseAttack: MELEE}, {row: 2, baseAttack: MAGIC}], benchSlot: 2, affiliation: ABSOLUTE_ADVERSARIES, alignment: LAWFUL_EVIL, race: 'Githyanki', class: 'Fighter', roles: [SUPPORT, DPS, SPEED], portrait: 'Portraits__Portrait_Laezel.png'
};

const astarion = {
    name: 'Astarion', baseAttack: RANGE, benchSlot: 10, affiliation: ABSOLUTE_ADVERSARIES, alignment: NEUTRAL_EVIL, race: 'Elf (High)', class: 'Rogue', roles: [SUPPORT, GOLD], portrait: 'Portraits__Portrait_Astarion.png'
};

const certainty = {
    name: 'Certainty Dran', baseAttack: MAGIC, benchSlot: 5, affiliation: ACQ_INC, alignment: NEUTRAL_GOOD, race: 'Half-Elf / Aasimar', class: 'Wizard / Bard', roles: [SUPPORT, GOLD], portrait: 'Portraits__Portrait_CertaintyDran.png'
};

const thellora = {
    name: 'Thellora', baseAttack: MELEE, benchSlot: 1, affiliation: NONE, alignment: LAWFUL_NEUTRAL, race: 'Centaur', class: 'Paladin', roles: [TANK, SUPPORT, SPEED], portrait: 'Portraits__Portrait_Thellora.png'
};

const jangsao = {
    name: 'Jang Sao', baseAttack: RANGE, benchSlot: 8, affiliation: NONE, alignment: NEUTRAL, race: 'Satyr', class: 'Druid / Wizard', roles: [HEALING, SUPPORT], portrait: 'Portraits__Portrait_JangSao.png'
};

const shadowheart = {
    name: 'Shadowheart', baseAttack: MAGIC, benchSlot: 6, affiliation: ABSOLUTE_ADVERSARIES, alignment: LAWFUL_NEUTRAL, race: 'Half-Elf', class: 'Cleric', roles: [HEALING, SUPPORT], portrait: 'Portraits__Portrait_Shadowheart.png'
};

const wyll = {
    name: 'Wyll', baseAttack: [{row: 1, baseAttack: MAGIC}, {row: 2, baseAttack: MELEE}], benchSlot: 12, affiliation: ABSOLUTE_ADVERSARIES, alignment: NEUTRAL_GOOD, race: 'Human', class: 'Warlock', roles: [SUPPORT], portrait: 'Portraits__Portrait_Wyll.png'
};

const karlack = {
    name: 'Karlach', baseAttack: MELEE, benchSlot: 4, affiliation: ABSOLUTE_ADVERSARIES, alignment: CHAOTIC_GOOD, race: 'Tiefling', class: 'Barbarian', roles: [DPS, TANK, SUPPORT], portrait: 'Portraits__Portrait_Karlach.png'
};

const presto = {
    name: 'Presto', baseAttack: MAGIC, benchSlot: 2, affiliation: SATURDAY_MORNING_SQUAD, alignment: NEUTRAL_GOOD, race: 'Human', class: 'Wizard', roles: [SUPPORT, DEBUFF], portrait: 'Portraits__Portrait_Presto.png'
};

const dynaheir = {
    name: 'Dynaheir', baseAttack: RANGE, benchSlot: 3, affiliation: BALDURS_GATE, alignment: LAWFUL_GOOD, race: 'Human', class: 'Wizard', roles: [SUPPORT, SPEED], portrait: 'Portraits__Portrait_Dynaheir.png'
};

const champions = {
    "legend_champion": bruenor,
    "core_champions": [bruenor, celeste, nayeli, jarlaxle, calliope, asharra, minsc, delina, makos, tyril, jamilah, arkhan],
    "evergreen_champions": [azaka, dragonbait, drizzt, hitch, nerds, reya, ulkoria, xerophon, blooshi, krux].sort((a, b) => a.name.localeCompare(b.name)),
    "event_champions": [
        aila, alyndra, artemis, avren, baeloth, barrowin, beadle_grimm, binwin, birdsong, blackviper,
        brig, briv, cattibrie, corazon, dhani, deekin, dhadius, donaar, ellywick, evelyn,
        ezmerelda, farideh, freely, gromma, havilar, hewmaan, ishi, jaheira, jim, korth,
        krond, krull, krydle, kthriss, lazaapz, lucius, mehen, melf, morgaen, nerys,
        nova, nrakk, omin, orisha, orkira, paultin, penelope, prudence, pwent, qillek,
        regis, rosie, selise, sentry, sgtknox, shaka, shandie, sisaspia, spurt, stoki,
        strix, talin, torogar, turiel, viconia, vlahnya, walnut, warden, widdle, wulfgar,
        xander, yorven, zorbu, rust, vi, desmond, tatyana, gazrick, dungeon_master, nordom,
        merilwen, nahara, valentine, voronika, dob, egbert, kent, virgil, warduke, imoen,
        fen, uriah, solaak, miria, antrius, nixie, evandra, bbeg, strongheart, vinursa,
        laezel, astarion, certainty, thellora, jangsao, shadowheart, wyll, karlack, presto, dynaheir
    ].sort((a, b) => a.name.localeCompare(b.name)),
    "events": [
        {
            row: 2, "name": "Highharvestide", "image": "../img/events/Icons_TimeGate__Icon_EventLogos_Highharvestide.png", isOldEvent: true,
            "champions": [stoki, farideh, pwent, torogar, dhani, egbert, certainty]
        },
        {
            row: 3, "name": "Liars' Night", "image": "../img/events/Icons_TimeGate__Icon_EventLogos_LiarsNight.png", isOldEvent: true,
            "champions": [krond, donaar, avren, ezmerelda, brig, kent, thellora]
        },
        {
            row: 4, "name": "Feast of the Moon", "image": "../img/events/Icons_TimeGate__Icon_EventLogos_FeastoftheMoon.png", isOldEvent: true,
            "champions": [gromma, vlahnya, sentry, penelope, widdle, virgil, jangsao]
        },
        {
            row: 5, "name": "Simril", "image": "../img/events/Icons_TimeGate__Icon_EventLogos_Simril.png",
            "champions": [dhadius, warden, krull, lucius, yorven, warduke, shadowheart]
        },
        {
            row: 6, "name": "Wintershield", "image": "../img/events/Icons_TimeGate__Icon_EventLogos_Wintershield.png",
            "champions": [barrowin, nerys, artemis, baeloth, viconia, imoen, wyll]
        },
        {
            row: 7, "name": "Midwinter", "image": "../img/events/Icons_TimeGate__Icon_EventLogos_Midwinter.png",
            "champions": [regis, kthriss, morgaen, talin, rust, fen, karlack]
        },
        {
            row: 8, "name": "Grand Revel", "image": "../img/events/Icons_TimeGate__Icon_EventLogos_GrandRevel.png",
            "champions": [birdsong, paultin, havilar, hewmaan, vi, uriah, presto]
        },
        {
            row: 9, "name": "Fleetswake", "image": "../img/events/Icons_TimeGate__Icon_EventLogos_Fleetswake.png",
            "champions": [zorbu, blackviper, sisaspia, orisha, desmond, solaak, dynaheir]
        },
        {
            row: 10, "name": "Festival of Fools", "image": "../img/events/Icons_TimeGate__Icon_EventLogos_FestivalofFools.png",
            "champions": [strix, rosie, briv, alyndra, tatyana, miria]
        },
        {
            row: 11, "name": "Greengrass", "image": "../img/events/Icons_TimeGate__Icon_EventLogos_Greengrass.png",
            "champions": [nrakk, aila, melf, orkira, gazrick, antrius]
        },
        {
            row: 12, "name": "The Running", "image": "../img/events/Icons_TimeGate__Icon_EventLogos_TheRunning.png",
            "champions": [cattibrie, spurt, krydle, shaka, dungeon_master, nixie]
        },
        {
            row: 13, "name": "The Great Modron March", "image": "../img/events/Icons_TimeGate__Icon_EventLogos_ModronMarch.png",
            "champions": [evelyn, qillek, jaheira, mehen, nordom, evandra]
        },
        {
            row: 14, "name": "Dragondown", "image": "../img/events/Icons_TimeGate__Icon_EventLogos_Dragondown.png",
            "champions": [binwin, korth, nova, selise, merilwen, bbeg]
        },
        {
            row: 15, "name": "Founders Day", "image": "../img/events/Icons_TimeGate__Icon_EventLogos_FoundersDay.png",
            "champions": [deekin, walnut, freely, sgtknox, nahara, strongheart]
        },
        {
            row: 16, "name": "Midsummer", "image": "../img/events/Icons_Events_2018Midsummer__Icon_EventLogos_Midsummer.png",
            "champions": [xander, shandie, beadle_grimm, ellywick, valentine, vinursa]
        },
        {
            row: 17, "name": "Ahghairon's Day", "image": "../img/events/Icons_Events_2018AhghaironsDay__AhghaironsDay_Icon_TimeGate.png",
            "champions": [ishi, jim, omin, prudence, voronika, laezel]
        },
        {
            row: 18, "name": "Brightswords", "image": "../img/events/Icons_Events_2018Brightswords__Brightswords_Icon_TimeGate.png",
            "champions": [wulfgar, turiel, lazaapz, corazon, dob, astarion]
        },
    ],
    "speed": [
        {
            "champion": deekin,
            "htmlText": "<span class='golden'>Deekin</span> is a <span class='golden'>Slot 1</span> support / speed champion whose &quot;<span class='golden'>Confidence in the Boss</span>&quot; ability (level 90) causes enemies to spawn more quickly under certain conditions. He also has a <span class='golden'>specialization</span> and a <span class='golden'>feat</span> that makes this even better!"
        },
        {
            "champion": thellora,
            "htmlText": "<span class='golden'>Thellora</span> is a <span class='golden'>Slot 1</span> support / tank / speed champion whose &quot;<span class='golden'>Plateaus of Unicorn Run</span>&quot; passive ability (level 0) causes areas to be skipped when Thellora kills her first enemy in an adventure, based on the number of areas Thellora completed since her last area skip. She also has a <span class='golden'>feat</span> that improves it!"
        },
        {
            "champion": widdle,
            "htmlText": "<span class='golden'>Widdle</span> is a <span class='golden'>Slot 2</span> support / healing / speed champion whose &quot;<span class='golden'>Tasty Friends</span>&quot; ability (level 260) causes enemies to spawn more quickly based on how many champions are being affected by her &quot;<span class='golden'>Vampiric Gaze</span>&quot; ability (which affects all adjacent champions)."
        },
        {
            "champion": laezel,
            "htmlText": "<span class='golden'>Lae'zel</span> is a <span class='golden'>Slot 2</span> support / dps / speed champion whose &quot;<span class='golden'>Straight to the Point</span>&quot; ability (level 100) can cause non-boss areas to be skipped if enemies are being defeated by anything other than Lae'zel. She also has two <span class='golden'>feats</span> for improving this!"
        },
        {
            "champion": bbeg,
            "htmlText": "<span class='golden'>BBEG</span> is a <span class='golden'>Slot 3</span> support / speed champion whose &quot;<span class='golden'>Railroad</span>&quot; ability (level 370) reduces the quest requirements of next area by 25% if you complete the current area in 5 seconds or less. He also has a <span class='golden'>feat</span> that doubles the reduction, literally halving the original quest requirements!"
        },
        {
            "champion": nahara,
            "htmlText": "<span class='golden'>Nahara</span> is a <span class='golden'>Slot 3</span> dps / speed champion whose &quot;<span class='golden'>To Amuse or Avenge</span>&quot; ability (level 90) reduces the quest requirements on each stage if she is landing killing blows. She also has a <span class='golden'>specialization</span> to increase this reduction by 50%."
        },
        {
            "champion": dynaheir,
            "htmlText": "<span class='golden'>Dynaheir</span> is a <span class='golden'>Slot 3</span> support / speed champion whose &quot;<span class='golden'>Spoils of War</span>&quot; ability (level 100) has a chance to cause Favored Foes to count double for quest progress. She has two <span class='golden'>feats</span>, and equipment for improving this!"
        },
        {
            "champion": sentry,
            "htmlText": "<span class='golden'>Sentry</span> is a <span class='golden'>Slot 4</span> support / tank / speed champion whose &quot;<span class='golden'>Create Echo</span>&quot; ability (level 60) has a chance to reduce quest requirements on each stage under certain conditions. She also has a <span class='golden'>feat</span> and <span class='golden'>equipment</span> slot for this!"
        },
        {
            "champion": briv,
            "htmlText": "<span class='golden'>Briv</span> is a <span class='golden'>Slot 5</span> support / tank / healing / speed champion whose &quot;<span class='golden'>Unnatural Haste</span>&quot; ability (level 80) has a chance to skip stages enterely under certain conditions. He also has a <span class='golden'>specialization</span> and <span class='golden'>equipment</span> slot to help with this!"
        },
        {
            "champion": xander,
            "htmlText": "<span class='golden'>Xander</span> is a <span class='golden'>Slot 5</span> support / speed champion whose &quot;<span class='golden'>Lucky Shot</span>&quot; ability has a low chance to make enemies award double quest progress when he lands the final blow. His &quot;<span class='golden'>Distraction</span>&quot; ability guarantees this bonus when he misses an attack."
        },
        {
            "champion": shandie,
            "htmlText": "<span class='golden'>Shandie</span> is a <span class='golden'>Slot 6</span> support / speed champion whose &quot;<span class='golden'>Dash</span>&quot; ability (level 120) increases game speed directly (like a <span class='golden'>Potion of Speed</span>) if the party hasn't been attacked for at least 30 seconds. She even has a <span class='golden'>feat</span> to double this effect! And a Season 2 feat which reduces the activation time from 30 to 10 seconds."
        },
        {
            "champion": minsc,
            "htmlText": "<span class='golden'>Minsc</span> is a <span class='golden'>Slot 7</span> DPS / support / speed champion whose &quot;<span class='golden'>Boastful</span>&quot; ability (level 50) has a small chance of causing one or two extra enemies to spawn whenever non-boss enemies spawn on a stage. It helps a little, but not much."
        },
        {
            "champion": hewmaan,
            "htmlText": "<span class='golden'>Hew Maan</span> is a <span class='golden'>Slot 8</span> support / speed champion whose &quot;<span class='golden'>Teamwork</span>&quot; ability (level 40) has a chance to cause enemies to count double for quest progress as long as <span class='golden'>Zrang</span> is the top kobold in the stack (front of formation). They also have two <span class='golden'>feats</span> and <span class='golden'>equipment</span> that increase this through &quot;<span class='golden'>Teamwork</span>&quot; ability (level 200), and even a <span class='golden'>feat</span> that increases the game speed by 10% if the party is not under attack."
        },
        {
            "champion": tatyana,
            "htmlText": "<span class='golden'>Tatyana</span> is a <span class='golden'>Slot 8</span> speed / support / tank champion whose &quot;<span class='golden'>Find a Feast</span>&quot; ability (level 80) makes her leave the formation for 2 seconds and come back with additional enemies that simultaneously spawn. This happens every 15 seconds she is not attacked. She has a <span class='golden'>feat</span> and <span class='golden'>equipment</span> for it!"
        },
        {
            "champion": havilar,
            "htmlText": "<span class='golden'>Havilar</span> is a <span class='golden'>Slot 10</span> support / tank / speed champion whose &quot;<span class='golden'>Fiendish Resolve</span>&quot; ability (level 165) causes fiend type enemies to count double for quest progress as long as she has <span class='golden'>Dembo</span> the Red Imp summoned to her side!"
        },
        {
            "champion": virgil,
            "htmlText": "<span class='golden'>Virgil</span> is a <span class='golden'>Slot 10</span> support / speed champion whose &quot;<span class='golden'>Rapid Fire</span>&quot; ability (level 100) has a chance to cause enemies to count double for quest progress as long as they are defeated within 2 seconds. He has a <span class='golden'>feat</span>, and <span class='golden'>equipment</span> for it!"
        },
        {
            "champion": melf,
            "htmlText": "<span class='golden'>Melf</span> is a <span class='golden'>Slot 12</span> support / speed champion whose &quot;<span class='golden'>Speedy Supplement</span>&quot; ability (level 70) can cause extra enemies to spawn, increase spawn speed, or increase quest progress speed! He has a <span class='golden'>specialization</span>, <span class='golden'>feat</span>, and <span class='golden'>equipment</span> for it!"
        },
        {
            "champion": vi,
            "htmlText": "<span class='golden'>Vi</span> is a <span class='golden'>Slot 12</span> support / gold / speed champion whose &quot;<span class='golden'>I'm Too Old For This #*&!</span>&quot; ability (level 90) can cause extra enemies to spawn if there are 5 or fewer enemies on screen! She has a <span class='golden'>feat</span>, and <span class='golden'>equipment</span> for it!"
        },
    ],
    "roles": [
        {
            "image": "../img/roles/Icons_Feats__Icon_Feat_SelfDPS.png",
            "name": "DPS",
            "text": "Champions who are designed to deal heavy damage."
        },
        {
            "image": "../img/roles/Icons_Feats__Icon_Feat_AttributeBoost.png",
            "name": "Support",
            "text": "Champions who help the team by boosting other champions."
        },
        {
            "image": "../img/roles/Icons_Feats__Icon_Feat_Overhelm.png",
            "name": "Tank",
            "text": "Champions who stay at the front to take hits for the team."
        },
        {
            "image": "../img/roles/Icons_Feats__Icon_Feat_Health.png",
            "name": "Healing",
            "text": "Champions who use their abilities to keep other champions alive."
        },
        {
            "image": "../img/roles/Icons_Feats__Icon_Feat_GoldFind.png",
            "name": "Gold",
            "text": "Champions who cause foes to drop more gold, and thus boost favor."
        },
        {
            "image": "../img/roles/speed.png",
            "name": "Speed",
            "text": "Champions who affect the speed of the game in different ways."
        },
        {
            "image": "../img/roles/Icons_Feats__Icon_Feat_AttributeBoost-debuff.png",
            "name": "Debuff",
            "text": "Champions who mark enemies with various special effects."
        },
    ],
    "affiliations": [
        {
            "name": ABSOLUTE_ADVERSARIES,
            "text": "Absolute Adversaries"
        },
        {
            "name": ACQ_INC,
            "text": "Acquisitions Incorporated"
        },
        {
            "name": AEROIS,
            "text": "Heroes of Aerois"
        },
        {
            "name": AWFUL_ONES,
            "text": "Awful Ones"
        },
        {
            "name": BALDURS_GATE,
            "text": "Heroes of Baldur's Gate"
        },
        {
            "name": BLACK_DICE_SOCIETY,
            "text": "Black Dice Society"
        },
        {
            "name": BRIMSTONE,
            "text": "Brimstone Angels"
        },
        {
            "name": C_TEAM,
            "text": 'Acq Inc "C" Team'
        },
        {
            "name": COMPANIONS,
            "text": "Companions of the Hall"
        },
        {
            "name": DARK_ORDER,
            "text": DARK_ORDER
        },
        {
            "name": FORCE_GREY,
            "text": FORCE_GREY
        },
        {
            "name": HOT_PLANES,
            "text": "Heroes of the Planes"
        },
        {
            "name": OXVENTURERS,
            "text": "Oxventurers Guild"
        },
        {
            "name": RIVALS,
            "text": "Rivals of Waterdeep"
        },
        {
            "name": SATURDAY_MORNING_SQUAD,
            "text": "Saturday Morning Squad"
        },
        {
            "name": SIRENS,
            "text": "Sirens of the Realms"
        },
        {
            "name": WAFFLE_CREW,
            "text": WAFFLE_CREW
        },
    ],
    "attackTypes": {
        MELEE: {
            "image": "../img/attacks/Icons_Attacks__Icon_DamageType_Melee.png",
            "name": "Melee",
        },
        MAGIC: {
            "image": "../img/attacks/Icons_Attacks__Icon_DamageType_Magic.png",
            "name": "Magic",
        },
        RANGE: {
            "image": "../img/attacks/Icons_Attacks__Icon_DamageType_Ranged.png",
            "name": "Range",
        }
    },
    "isMelee": melee, "isMagic": magic, "isRange": range,
    "isDps": dps, "isSupport": support, "isTank": tank, "isHealing": healing, "isGold": gold, "isSpeed": speed, "isDebuff": debuff,
};

champions.event_years = [...Array(Math.max(...champions.events.map(event => event.champions.length))).keys()].map(year => 'Y'+(year+1));

function renderCoreChampions() {
    const templateChampion = document.getElementById('template_champion').innerHTML;
    Mustache.parse(templateChampion);
    const templateChampionBeadleAndGrimm = document.getElementById('template_champion_beadle_and_grimm').innerHTML;
    Mustache.parse(templateChampionBeadleAndGrimm);
    const templateLegendChampion = document.getElementById('template_legend_champion').innerHTML;
    Mustache.parse(templateLegendChampion);
    const templateCoreChampions = document.getElementById('template_core_champions').innerHTML;
    Mustache.parse(templateCoreChampions);
    const templateEvergreenChampions = document.getElementById('template_evergreen_champions').innerHTML;
    Mustache.parse(templateEvergreenChampions);
    const templateEventChampions = document.getElementById('template_event_champions').innerHTML;
    Mustache.parse(templateEventChampions);
    const templateCalendarChampions = document.getElementById('template_calendar_champions').innerHTML;
    Mustache.parse(templateCalendarChampions);
    const templateSpeedChampions = document.getElementById('template_speed_champions').innerHTML;
    Mustache.parse(templateSpeedChampions);
    const templateRolesListing = document.getElementById('template_roles_listing').innerHTML;
    Mustache.parse(templateRolesListing);
    const templateAffiliationsListing = document.getElementById('template_affiliations_listing').innerHTML;
    Mustache.parse(templateAffiliationsListing);
    document.getElementById('legend_champion').innerHTML = Mustache.render(templateLegendChampion, champions, {"template_champion": templateChampion});
    document.getElementById('core_champions').innerHTML = Mustache.render(templateCoreChampions, champions, {"template_champion": templateChampion});
    document.getElementById('evergreen_champions').innerHTML = Mustache.render(templateEvergreenChampions, champions, {"template_champion": templateChampion});
    document.getElementById('event_champions').innerHTML = Mustache.render(templateEventChampions, champions, {"template_champion": templateChampion, "template_champion_beadle_and_grimm": templateChampionBeadleAndGrimm});
    document.getElementById('calendar_champions').innerHTML = Mustache.render(templateCalendarChampions, champions, {"template_champion": templateChampion});
    document.getElementById('speed_champions').innerHTML = Mustache.render(templateSpeedChampions, champions, {"template_champion": templateChampion});
    document.getElementById('roles_listing').innerHTML = Mustache.render(templateRolesListing, champions, {"template_champion": templateChampion});
    document.getElementById('affiliations_listing').innerHTML = Mustache.render(templateAffiliationsListing, champions, {"template_champion": templateChampion});
}
